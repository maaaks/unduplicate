#!/usr/bin/python2.7
# coding: utf8

import hashlib
import os
import sys

from PySide.QtCore import Qt
from PySide.QtCore import QSize
from PySide.QtGui import QApplication
from PySide.QtGui import QDialog
from PySide.QtGui import QHBoxLayout
from PySide.QtGui import QIcon
from PySide.QtGui import QLabel
from PySide.QtGui import QMessageBox
from PySide.QtGui import QProgressDialog
from PySide.QtGui import QPushButton
from PySide.QtGui import QSizePolicy
from PySide.QtGui import QSpacerItem
from PySide.QtGui import QToolButton
from PySide.QtGui import QVBoxLayout


class UnduplicateDialog(QDialog):
	
	def __init__(self, filenames):
		QDialog.__init__(self)
		self.filenames = filenames
		
		# Window title
		self.setWindowTitle(self.trUtf8( str(len(filenames)) + " icons — Unduplicate" ))
		self.setWindowIcon(QIcon.fromTheme("edit-copy"))
		
		# Main vertical layout
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		
		# Message on the top
		message = QLabel(self.trUtf8("Эти файлы, кажется, одинаковые.\nВыберите один из них, чтобы удалить остальные."))
		message.setAlignment(Qt.AlignCenter)
		mainLayout.addWidget(message)
		
		# Buttons for each file, divided in lines of 4 buttons
		for index, filename in enumerate(filenames):
			if index % 4 == 0:
				buttonsLayout = QHBoxLayout()
				mainLayout.addLayout(buttonsLayout)
				
			buttonsLayout.addLayout(self.buttonsForFile(filename))
			
			if index % 4 == 3 or index == len(filenames)-1:
				buttonsLayout.addItem(QSpacerItem(0, 0, QSizePolicy.Expanding))
				
		# Bottom horizontal layout
		bottomLayout = QHBoxLayout()
		mainLayout.addLayout(bottomLayout)
		
		# Spacer to move buttons left
		bottomLayout.addStretch()
		
		# About button
		aboutButton = QPushButton(QIcon.fromTheme("dialog-information"), self.trUtf8("О программе"))
		aboutButton.clicked.connect(self.about)
		bottomLayout.addWidget(aboutButton)
		
		# Stop button
		closeButton = QPushButton(QIcon.fromTheme("application-exit"), self.trUtf8("Выйти"))
		closeButton.clicked.connect(sys.exit)
		bottomLayout.addWidget(closeButton)
		
		
	def buttonsForFile(self, filename):
		"""
		Returns QLayout with buttons for given filename
		"""
		layout = QVBoxLayout()
		layout.setSpacing(0)
		
		# Button to choose the file
		button = QToolButton()
		button.setObjectName(filename)
		button.setText(filename)
		button.setIcon(QIcon(sys.argv[1] + os.sep + filename))
		button.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
		button.setIconSize(QSize(128, 128))
		button.clicked.connect(self.removeAllExcept)
		layout.addWidget(button)
		
		return layout
		
		
	def removeAllExcept(self, checked=True):
		"""
		Remove all the files except the one specified in the sender's objectName
		"""
		
		# Get the filename which was chosem
		chosenfilename = self.sender().objectName()
		
		# Remove all files from the list
		for filename in self.filenames:
			if filename is not chosenfilename:
				os.remove(sys.argv[1] + os.sep + filename)
		
		# Close current dialog
		self.close()
		
		
	def about(self):
		"""
		Show information about me
		"""
		QMessageBox.information(self, "Unduplicate", self.trUtf8(
				"<h1>Unduplicate 1.0</h1>\
				<h3>Утилита для избавления от повторяющихся картинок</h3>\
				Автор: greatperson, август 2011.<br/>\
				При использовании кода ссылка на <a href='http://maaaks.ru/'>Maaaks.ru</a> в каком-нибудь похожем окне обязательна.<br/>"
		))


class UnduplicateApplication(QApplication):
	
	def __init__(self, argv):
		QApplication.__init__(self, argv)
		
		# Prepare dictionary for saving filenames with hashes as keys
		files = os.listdir(sys.argv[1])
		files_by_groups = {}
		
		# Create progress dialog
		progress = QProgressDialog()
		progress.setLabelText(self.trUtf8("Сканирование..."))
		progress.setMinimum(0)
		progress.setMaximum(len(files))
		progress.setCancelButton(None)
		progress.setMinimumDuration(0)
		
		# Walk through list of files in the directory
		for i, filename in enumerate(files, start=1):
			hash = self.filehash(sys.argv[1] + os.sep + filename)
			
			if not files_by_groups.has_key(hash):
				files_by_groups[hash] = []
			files_by_groups[hash].append(filename)
			
			progress.setValue(i)
			
		# Ask questions about those sets where there are more then one file
		for group_name in files_by_groups.keys():
			group = files_by_groups[group_name]
			if len(group) > 1:
				UnduplicateDialog(group).exec_()
		
		
	def buttonForFile(self, filename):
		"""
		Returns a QPushButton with image of a file
		"""
		return QPushButton(filename)
		
		
	def filehash(self, filename):
		"""
		Returns hash of the file with given name
		"""
		content = open(filename).read()
		return hashlib.md5(content).hexdigest()


##################################
# So, let's start!
##################################

app = UnduplicateApplication(sys.argv)
app.exec_()